<?php
    //use Doctrine\ORM\Tools\Setup;
    //use Doctrine\ORM\EntityManager;

    require_once dirname(__FILE__) . '/../vendor/autoload.php' ;
    
    
    $isDevMode = true;
    
    
    $config = ['settings' => [
        'addContentLengthHeader' => false,
        'displayErrorDetails' => true,
        ]];

    $app = new \Slim\App($config);


    //SESSION STUFF
    $app->add(new \Slim\Middleware\Session([
        'name' => 'urkl_session',
        'autorefresh' => true,
        'lifetime' => '1 hour'
    ]));


    // Get container
    $container = $app->getContainer();

    //MORE SESSION STUFF
    $container['session'] = function ($c) {
        return new \SlimSession\Helper;
    };

    // Register component on container
    $container['view'] = function ($container) {
    $view = new \Slim\Views\Twig(__DIR__ . '/template', [
        'cache' =>  false // __DIR__ . '../share/cache'
    ]);
    
    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));

    return $view;
};

