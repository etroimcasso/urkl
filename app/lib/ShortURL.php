<?php
//namespace Urkl;
/**
* @Entity @Table(name="urls")
* 
*/
class ShortURL {
    
	/**
	* @Id @Column(type="string")
	* @var string
	*/
	protected $id;
     
	/**
	* @Column(type="string")
	* @var string
	*/
	protected $url;
    

	public function getId() {
		return $this->id;
	}
      
	public function getUrl() {
		return $this->url;
	}
	
	public function setId($id) {
   		$this->id = $id;
    } 
      
	public function setUrl($url) {
		$this->url = $url;
	}
    
}