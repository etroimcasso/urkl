<?php namespace Urkl\Controller;
require_once dirname(__FILE__) . '/../../bootstrap.php';


/*
This class handles the creation and storage / retrieval / updating of website screenshots.

Screenshots are stored in the public/storage/screenshots folder, which is a symlink to /app/storage/screenshots.
Screenshots files are named after their entry's shortcode.
Duplicate entries are linked to the first screenshot for that URL, and the image is updated
This will be done via a special "image_id" entry in mongodb database that can be different than the shortcode

This class will be called during the creation of any new links. The process is as follows:
   1. Class object will take input of URL and a shortcode
   2. Class object checks database for duplicates of URL
   3a. If URL exists, update image and output shortcode of original URL
   3b. If URL doesn't exist, create new image and output same shortcode as input

CLASS OUTPUT IS ONLY A SHORTCODE: Either the shortcode of the existing image for duplicate entries, 
  or the shortcode from the original input for new URLs


+(Completed) Shortcode controller will need modifications to create a new entry for "image_id"
-(In Progress) Templates will need to be updated to display the images in image_id


/// FUTURE FEATURES:
///   -When original image entry is deleted, rename image to the next oldest shortcode entry linking to that image
///   -Images are updated whenever a user uses the link, but only if it's been at least one day (week? Month? two weeks?) since the last update



/* NOTES
    Currently the Screenshotting system will use the Google PageSpeed Insights API, at least during testing.

    TODO: Create own screenshotting system that basically renders pages offscreen, screenshots them, and then saves and catalogs them the usual way, as described in the previous notes on this class.

*/

class ScreenshotController {

    public function __construct() {

    }

    //Function takes a URL and a shortcode as input, outputs shortcode
    //Checks if the URL is a duplicate, takes the screenshot after
    //Screenshots are actually taken in the javascript using PhantomJS. This function only
    //returns the shortcode (filename) the screenshot is saved as. 
    public function getScreenshotID($url, $shortcode) {
        //For now, return $shortcode        
        return $shortcode;


        //Check if URL is a duplicate.
        //If URL is duplicate, find the oldest duplicate and use that shortcode
        //Create a new screenshot of that URL, and update the image at the shortcode with the new screenshot (this will be done in javascript automatically)
        //return shortcode

    }

    //This function might not be needed. The user interface can work with just image_id if the locations are hard-coded, which
    //shouldn't be an issue in this instance since this application isn't complex.
    //Hardcoding storage locations shouldn't be an issue
    public function getFullImagePath($shortcode) {
        //Uses shortcode to retrieve image_id from shortcode.
        //Returns the full URL of the image associated with the shortcode
    }

    private function takeScreenshot($url, $shortcode) {
        //Use the URL to take a screenshot, give it the shortcode name

    }

    private function updateScreenshot($shortcode) {
        //Get the URL from the shortcode
        // Will basically call takeScreenshot with the URL retrieved from the shortcode

    }


    private function isDuplicate($url) {
        //Checks if the URL exists. If it does, return the shortcode, otherwise return FALSE

    }

}