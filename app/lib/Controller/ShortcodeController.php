<?php namespace Urkl\Controller;
require_once dirname(__FILE__) . '/../../bootstrap.php';


//Takes a URL and generates a shortcode for it, then adds that URL + shortcode combo to the database
//Also checks if current URL exists already, and returns the shortcode for the URL that exists already.
//  Otherwise, returns new shortcode
//Shortcode is generated character-by-character. 
//Shortcode is $shortCodeLength n characters, 0-9,A-Z,a-z (60^n possibilities)
//If generated shortcode exists already, generate new one until it is unique


class ShortcodeController {
    //Shortcode is generated character-by-character. 
    //Shortcode is $shortCodeLength n characters, 0-9,A-Z,a-z (62^n possibilities)
    //If generated shortcode exists already, generate new one until it is unique
    
    //Determines the length of the shortcode. Possibile combos are 62^shortCodeLength
    private $shortCodeLength = 6;

    private $db_username = "mongoadmin";
    private $db_password = '12KRTh18G9g$$uGGoi$';
    private $db_name = "urkl";

    private $connect_string;
    private $db;


    //private $links;

    public function __construct() {
      //$links = (new \MongoDB\Client)->urkl->links;
        $this->connect_string = "mongodb://" 
            . $this->db_username . ":" 
            . $this->db_password 
            . "@localhost:27017";
        $this->db = new \MongoDB\Client($this->connect_string);
    }

    public function getShortCodeLength() {
        return $this->shortCodeLength;
    }
    
    public function getAllLinks() {
        return $this->db->urkl->links->find();
    }
    
    public function getURL($shortcode) {
        return (new \Urkl\Controller\DatabaseController)->getLink($shortcode);
    }

    public function getRemainingPossibilities() {
        $total_pos = 62 ** $this->shortCodeLength; 
        return $total_pos - ($this->db->urkl->links->count());
    }

    public function getUniqueShortCode() {
        $shortcode = $this->generateShortCode();
        $isUnique = $this->isShortcodeUnique($shortcode);

        //Loops until shortcode is found to be unique
        while ($isUnique == false) {
                $shortcode = $this->generateShortCode();
                $isUnique = $this->isShortcodeUnique($shortcode);
        }

        return $shortcode;
    }

    public function isShortcodeUnique($shortcode) {
        $isUnique = false;

        //Loops until shortcode is found to be unique
        while ($isUnique == false) {
            if ( is_null($this->db->urkl->links->findOne(['shortcode' => $shortcode])) ) { 
                //If results are empty, shortcode is unique
                return true;
            } else {
                //Shortcode is not unique, generate another one
                return false;            }
        }

    }
        
    private function generateShortCode() {
        $shortCode = array(); //This array will hold the $shortCodeLength n characters needed
        
        //Start at 0
        for ($i = 0; $i < $this->shortCodeLength; $i++) {
            //Randomize between 0 and 2 for "uppercase, lowercase, number"
            switch (random_int(0,2)) {
            case 0:
                //uppercase
                $char = chr(random_int(65,90));
                break;
            case 1:
                //lowercase
                $char = chr(random_int(97,122));
                break;
            case 2:
                //number
                $char = random_int(0,9);
                break;
            }
            //Add the generated character to the shortcode array
            $shortCode[$i] = $char;
        }
        
        return implode($shortCode);
    }

    
    //Returns true or false
    private function isUrlInUse($url) {
        //Check database for URL.
    }
    
    public function createNewURL($url,$shortcode,$title, $desc, $public, $image_id) {
        //Check if URL already exists
        //Add to database

        if ($this->isShortcodeUnique($shortcode)) {
            try {
                $this->db->urkl->links->insertOne($this->createShortLinkDBArray($shortcode,$url,$title,$desc,$image_id,0), array("w" => 1));
                return true;
            } catch(MongoCursorException $e) {
                return false;
            }
        } else {
            return false;
        }

        //(new \MongoDB\Client)->urkl->links->insertOne($this->createShortLinkDBArray($shortcode,$url,$desc,0), array("w" => 1));
        //return true; //For now, returns true when it worked
    }

    //Used when only the URL is wanted from a shortcode. 
    public function getLink($shortcode) {
        return $this->db->urkl->links->findOne(['shortcode' => $shortcode], ['id' => false, 'url' => true]);
    }


    public function increaseViews($shortCode) {
        $this->db->urkl->links->updateOne(
            [ 'shortcode' => $shortCode ],
            [ '$inc'=> [ 'views' => 1 ]]
        );
    }

    //Whatever function calls this one needs to check the integrity of the data presented to it before passing things off to this
    //and presumably (wait how is it presumably if I'm the only one writing it?) creating a new document in the database with the information 
    private function createShortLinkDBArray($shortcode,$url, $title, $desc, $image_id, $user) {
        $link = array (
            'shortcode' => $shortcode,
            'url' => $url,
            'views' => 0,
            'rating' => 0,
            'created_at' => new \MongoDB\BSON\UTCDateTime(),
            'public' => true,
            'page_title' => $title,
            'last_retrieved' => new \MongoDB\BSON\UTCDateTime(),
            'desc' => $desc,
            'owner' => $user,
            'image_id' => $image_id
            );
        return $link;
    }

    public function getPageTitleFromURL($url) {
        /*
        $file = file_get_contents($url);
        foreach($file->find('title') as $element)
            return $element;
            */

        $str = file_get_contents($url);
        if(strlen($str)>0){
            if (strcmp($str,"") != 0) {
                $str = trim(preg_replace('/\s+/', ' ', $str)); // supports line breaks inside <title>
                preg_match("/\<title\>(.*)\<\/title\>/i",$str,$title); // ignore case
                return $title[1];
            } else {
                return "Title Could not be Retrieved";
            }
        } else {
            return "No title";
        }
    }

}