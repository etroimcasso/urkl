<?php 
use Slim\Http\Request;
use Slim\Http\Response;


    require_once dirname(__FILE__) . '/../bootstrap.php';

/*
    $app->get('/', function($request, $response, $args) {
            return $response->withRedirect('/all');

    });
    */
    
    $app->get('/', function ($request, $response, $args) {
        $sc = new Urkl\Controller\ShortcodeController();

        /*

            if ($request->getAttribute('newlink_added') == true) {

                $add_success = $request->getAttribute('add_success');

                $links =  $sc->getAllLinks();

                return $this->view->render($response,'all_links.html.twig',[
                                    'sc_length' => $sc->getShortCodeLength(),
                                    'old_shortcode' => $request->getAttribute('old_shortcode'), 
                                    'old_url' => $request->getAttribute('old_url'),
                                    'add_success' => $add_success,
                                    'is_error' => !$add_success,
                                    'links' => $links,
                                   // 'basePath' => $request->getUri()
                                    ]);
            } 
        */


            return $this->view->render($response,'all_links.html.twig',[
                                    'sc_length' => $sc->getShortCodeLength(),
                                    'links' => $sc->getAllLinks(), 
                                  //  'basePath' => $request->getUri()
                                    ]);
    });
    

    $app->get('/about', function ($request, $response, $args) {
        return $this->view->render($response,'about.html.twig');
    });
    
    $app->get('/{shortcode}', function ($request, $response, $args) {
        $sc = new Urkl\Controller\ShortcodeController();
        
        $sc->increaseViews($args['shortcode']);
        
       // return $this->view->render($response,'redirect_frame.html.twig',['link' => $db->getLink($args['shortcode'])]);
       return $response->withRedirect($sc->getLink($args['shortcode'])->url);
    });
    
    $app->get('/api/i/{shortcode}', function ($request, $response, $args) {
        $sc = new Urkl\Controller\ShortcodeController();
        //$db = new Urkl\Controller\DatabaseController();
        
        return $this->view->render($response,'link-info.html.twig',['link' => $sc->getLink($args['shortcode']), 'shortcode' => $args['shortcode']]);

    });
    

    //POST routes

    $app->post('/api/new/user', function($request, $response, $args) {



        /*
            Create new user
        */

    });

    $app->post('/api/new_url', function($request, $response, $args) {
        $sc = new Urkl\Controller\ShortcodeController();
        
        $url = $request->getParam('url');
        $shortcode = $request->getParam('shortcode');
        $title = $request->getParam('title');
        $desc = $request->getParam('desc');
        $is_public = $request->getParam('is_public');
        $user = $request->getParam('user');
        $image_id = $request->getParam('image_id');

        $add_success = $sc->createNewURL($url, $shortcode, $title, $desc, $is_public, $image_id);

        if ($add_success == true) {
            return $response->withJson(array('success' => true));
        } else {
            return $response->withJson(array('success' => false));
        }

    });

    //GET routes

    //Generate shortcode
    $app->get('/api/shortcode/generate', function($request, $response, $args) {
        $shortcode = (new Urkl\Controller\ShortcodeController)->getUniqueShortCode();
        return $response->withJson(array('response' => $shortcode ));
    });

    //Detect title
    $app->get('/api/shortcode/detect_title', function($request, $response, $args) {
        $title = (new Urkl\Controller\ShortcodeController)->getPageTitleFromURL($request->getParam('url'));
        return $response->withJson(array('response' => $title));
    });

    //Get image_id
    //  Request requires url and shortcode parameters
    $app->get('/api/shortcode/image_id', function($request, $response, $args) {
        $url = $request->getParam('url');
        $shortcode = $request->getParam('shortcode');
        return $response->withJson(array('response' => (new Urkl\Controller\ScreenshotController)->getScreenshotID($url, $shortcode)));
    });


    /* Returns a list of ALL shortcode objects sorted from oldest to newest in JSON format
       /api/shortcode/lists/all
    */
    $app->get('/api/shortcode/lists/all', function($request, $response, $args) {
        return $response->withJson(array('response' => (new Urkl\Controller\ShortcodeController)->getAllLinks()->toArray()));
    });

    //Returns the required shortcode length
    $app->get('/api/shortcode/params/length', function($request, $response, $args) {
        return $response->withJson(array('response' => (new Urkl\Controller\ShortcodeController)->getShortCodeLength()));


    });

    //Returns information on the specified link (shortcode)
    $app->get('/api/shortcode/link_info', function($request, $response, $args) {

    });


    //Returns the number of remaining unique shortcodes
    $app->get('/api/shortcode/params/remain', function($request, $response, $args) {
        return $response->withJson(array('response' => (new Urkl\Controller\ShortcodeController)->getRemainingPossibilities()));
    });

    //Returns a list of all users
    $app->get('/api/users/lists/all', function($request, $response, $args) {


    });

    //PATCH routes

    //DELETE routes
    
    //For the shortcode redirection, take a route with example.com/<shortcode>
    //Search database for shortcode,
    //  If exists, then redirect to the URL with the shortcode as its ID
    
    $app->run();