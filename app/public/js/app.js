//Initializes the app info popup for the topbar's URKL text
$('#_app_info_').popup();

//Initializes all sticky UI items
//Since the only sticky UI element for now is the topbar, this declaration call refer to all stickies
$('.ui.sticky').sticky({
	context: '#urkl-main-view'
});

//Initializes all checkboxes
$('.ui.checkbox')
  .checkbox({
  })
;

/*
//Initializes ratings component and disables user interaction
$('.ui.rating').rating('disable',true);

$('.ui.icon.button.link_desc')
  .popup({
  	on: 'click'
  })
;
*/

//Makes messages closable
$('.message .close')
  .on('click', function() {
    $(this)
      .closest('.message')
      .transition('fade')
    ;
  })
;

//New Link Modal and Form Stuff
$('.newlink.modal').modal('attach events', '.item.newlink','show');
$('.newlink.modal').modal('attach events', '#cancel_newlink_button','hide');
$('.newlink.modal').modal('setting','closable',true);

$('.ui.form.newlinkform')
	.form({
		on: 'blur',
		fields: {
			url: {
				identifier: 'url',
				rules: [
					{
						type: 'url',
						prompt: 'Must be a valid url'
					}
				]
			},
			title: {
				identifier: 'title',
				rules: [
					{
						type: 'minLength[3]',
						prompt: 'Title must be at least 5 characters'
					}
				]
			},

			desc: {
				identifier: 'desc',
				rules: [
					{
						type: 'maxLength[255]',
						prompt: 'Description cannot be longer than 255 characters'
					}
				]


			}
		}
	})
;

//Submit new URL 
$('#submit_newlink_button').click(function(event) {
	var $form = $('.ui.form.newlinkform');
	var public = false;
	if ( $form.form('is valid') == true) {
		var 
			n_url = $form.form('get value', 'url'),
			shortcode = $form.form('get value', 'shortcode'),
			desc = $form.form('get value', 'desc');
			title = $form.form('get value', 'title');

		if ($('.is_public_check').checkbox('is checked')) {
			public = true;
		}

		/* To get the image_id functionality to work properly:
			1. After form is validated, send GET request to the api address that returns the results
			   of the ScreenshotController's shortcode 
			2. Call the /api/new_url ajax within the success thing of the previous ajax, but call it with the new
			   image_id data as well. 
		*/

		//console.debug(n_url);

		$.ajax({
			url: "/api/shortcode/image_id",
			type: 'GET',
			data: {
				url: n_url,
				shortcode: shortcode
			},
			success: function(result) {
				image_id = result.response;


				//Now it needs to take a screenshot

				$.ajax({
					url: "/api/new_url",
					type: 'POST',
					data: {
						url: n_url,
						shortcode: shortcode,
						desc: desc,
						title: title,
						is_public: public,
						image_id: image_id
					},
					success: function(result) {
						if (result.success == true) {
							//Reset form
							//$form.form('reset');
							$form.form('clear');

							//Close modal
							$('.newlink.modal').modal('hide');
							location.reload();

						} else {
							console.debug ('failure');
						}
					}

				})
			}
		})
	}

});

//Shortcode stuff

////Get new shortcode on document load 
$('.item.newlink').click(function() {
	$.ajax({
		url: "/api/shortcode/generate",
		type: 'GET',

		success: function(result) {
			$("#shortcode_input").val(result.response);
		}
	})
});


///
$('.item.profile_button').click(function() {



})

////Shortcode button refresh with AJAX
$( '#refresh_shortcode_button' ).click(function( event ) {

	$('#shortcode_input_div').addClass("loading");

	//Retrieve new shortcode
	$.ajax({
		url: "/api/shortcode/generate",
		type: 'GET',
		success: function(result) {
			$("#shortcode_input").val(result.response);
			$('#shortcode_input_div').removeClass("loading");
		}
	})


});

////Detect title button, refresh with AJAX, disables button after initial detection
//$('#title_input').on('input',function(event) {
$('#detect_title_button').click(function(event) {

	var $form = $('.ui.form.newlinkform');

	if ($('.ui.form.newlinkform').form('is valid', 'url')) {

		n_url = $form.form('get value', 'url'),


		$('#title_input_div').addClass("loading");

		//Retrieve new shortcode
		$.ajax({
			url: "/api/shortcode/detect_title",
			type: 'GET',
			data: { url: n_url },
			success: function(result) {
				$("#title_input").val(result.response);
				$('#title_input_div').removeClass("loading");
			}
		});
	}

});

function takeURLScreenshot(url, shortcode) {
	//Will call PhantomJS to save a screenshot of 'url' with filename 'shortcode'
}

//Copy link to clipboard when clicking shortcode in link items

