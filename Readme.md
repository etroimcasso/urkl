--Apache 
----must follow symlinks
----must have mod_rewrite enabled


--Symlink from the app/public/storage folder to app/storage
----Folder must be readable by all, writeable by user and group



USES 
-Slim framework
-Browsershot @ https://github.com/spatie/browsershot - MIT LICENSE
-Semantic UI
-PhantomJS
-PhantomJS Installer @ https://github.com/jakoch/phantomjs-installer

PHP requires:
-bz2


Linux requires:
-PHP 7
-fontconfig
